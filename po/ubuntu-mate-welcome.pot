# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-09 18:01+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ubuntu-mate-welcome:119
msgid "Fixing incomplete install succeeded"
msgstr ""

#: ubuntu-mate-welcome:120
msgid "Successfully fixed an incomplete install."
msgstr ""

#: ubuntu-mate-welcome:120
msgid "Fixing the incomplete install was successful."
msgstr ""

#: ubuntu-mate-welcome:124
msgid "Fixing incomplete install failed"
msgstr ""

#: ubuntu-mate-welcome:125
msgid "Failed to fix incomplete install."
msgstr ""

#: ubuntu-mate-welcome:125
msgid "Fixing the incomplete install failed."
msgstr ""

#: ubuntu-mate-welcome:132
msgid "Fixing broken dependencies succeeded"
msgstr ""

#: ubuntu-mate-welcome:133
msgid "Successfully fixed broken dependencies."
msgstr ""

#: ubuntu-mate-welcome:133
msgid "Fixing the broken dependencies was successful."
msgstr ""

#: ubuntu-mate-welcome:137
msgid "Fixing broken dependencies failed"
msgstr ""

#: ubuntu-mate-welcome:138
msgid "Failed to fix broken dependencies."
msgstr ""

#: ubuntu-mate-welcome:138
msgid "Fixing the broken dependencies failed."
msgstr ""

#: ubuntu-mate-welcome:191
msgid "Install"
msgstr ""

#: ubuntu-mate-welcome:192
msgid "Installation of "
msgstr ""

#: ubuntu-mate-welcome:193
msgid "installed."
msgstr ""

#: ubuntu-mate-welcome:195
msgid "Remove"
msgstr ""

#: ubuntu-mate-welcome:196
msgid "Removal of "
msgstr ""

#: ubuntu-mate-welcome:197
msgid "removed."
msgstr ""

#: ubuntu-mate-welcome:199
msgid "Upgrade"
msgstr ""

#: ubuntu-mate-welcome:200
msgid "Upgrade of "
msgstr ""

#: ubuntu-mate-welcome:201
msgid "upgraded."
msgstr ""

#: ubuntu-mate-welcome:206 ubuntu-mate-welcome:207
msgid "complete"
msgstr ""

#: ubuntu-mate-welcome:207
msgid "has been successfully "
msgstr ""

#: ubuntu-mate-welcome:209 ubuntu-mate-welcome:210
msgid "cancelled"
msgstr ""

#: ubuntu-mate-welcome:210
msgid "was cancelled."
msgstr ""

#: ubuntu-mate-welcome:212 ubuntu-mate-welcome:213
msgid "failed"
msgstr ""

#: ubuntu-mate-welcome:213
msgid "failed."
msgstr ""
